from vector import Vector3
from mesh import *

def parseOBJ(filename):
    with open(filename, "r") as ins:
        vertexarray = []
        polygons = []
        for line in ins:
            a = line.split(' ')
            if(a[0] == 'v'): #we have a vertex
                vertexarray.append(Vector3(float(a[1]),float(a[2]),float(a[3])))
            if(a[0] == 'f'): #we have a polygon
                #print(int(a[1].split('/')[0])-1)
                poly = polygon(vertexarray[int(a[1].split('/')[0])-1],
                               vertexarray[int(a[2].split('/')[0])-1],
                               vertexarray[int(a[3].split('/')[0])-1])
                polygons.append(poly)
    return mesh(polygons)




def main():
    a = parseOBJ("cylinder.obj")
    print(a)

if __name__ == '__main__':
    main()
