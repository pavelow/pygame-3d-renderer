# /usr/bin/python
#This file contains Vector Object and Operations
import math

class Vector3:
	def __init__(self, X, Y, Z):
		self.X = X
		self.Y = Y
		self.Z = Z

	def __str__(self):
		return "[{0},{1},{2}]".format(self.X, self.Y, self.Z)

	def __add__(self, other):
		return Vector3(self.X + other.X, self.Y + other.Y, self.Z + other.Z)

	def __sub__(self, other):
		return Vector3(self.X - other.X, self.Y - other.Y, self.Z - other.Z)

	def __mul__(self, other):
		return Vector3(self.X * other.X, self.Y * other.Y, self.Z * other.Z)

	def __eq__(self, other):
		return (self.X == other.X and self.Y == other.Y and self.Z == other.Z)

	def scale(self, other):
		return Vector3(self.X*other, self.Y*other, self.Z*other)

	def magnitude(self):
		return math.sqrt(self.X**2 + self.Y ** 2 + self.Z ** 2)

	def normalize(self):
		len = self.magnitude()
		return Vector3(self.X/len, self.Y/len, self.Z/len)

	def dot(self, other):
		#a · b = ax × bx + ay × by
		return self.X*other.X + self.Y*other.Y + self.Z*other.Z

	def cross(self, other):
		#cx = aybz − azby
		Cx = self.Y*other.Z - self.Z*other.Y
		#cy = azbx − axbz
		Cy = self.Z*other.X - self.X*other.Z
		#cz = axby − aybx
		Cz = self.X*other.Y - self.Y*other.X
		return Vector3(Cx, Cy, Cz)

		#a × b = |a| |b| sin(θ) n
	def angle(self,other):
		d = self.dot(other)
		p = self.magnitude() * other.magnitude()
		return math.acos(d/p)

	def angledeg(self, other):
		a = self.angle(other)
		return a/(math.pi/180)

	def distance(self, other):
		return (self - other).magnitude()

	#Vector projected on another (magnitude)
	def project(self, other):
		return(self.dot(other)/other.magnitude())

	def rotate(self, axis, theta):
		a_p = axis.normalize().scale(self.project(axis)) #Component along axis does not change
		a_o = self - a_p #Orthogonal component
		a_o_mag = a_o.magnitude()
		if(a_o_mag != 0):
			omega = axis.cross(a_o)
			x1 = math.cos(theta)/a_o_mag
			x2 = math.sin(theta)/omega.magnitude()
			a_rotate = (a_o.scale(x1) + omega.scale(x2)).scale(a_o_mag)
			return a_rotate + a_p
		else:
			return self #if a_o magnitude is zero, vector is parallel to axis

def main():
	import time
	a = Vector3(4,8,10)
	b = Vector3(9,2,7)
	print(a.magnitude()) #Should be 5
	c = a.normalize()
	print(c.magnitude()) #Should be 1
	d = a + a
	print(d.magnitude()) #Should be 10
	print(a == a) #Should be True
	print(a.dot(b)) #Should be 122
	print(a.angledeg(b)) #Should be 38.2

	e = Vector3(2,3,4)
	f = Vector3(5,6,7)
	print(e.cross(f))  #e × f = (−3,6,−3)rr

	print(a.distance(b))

	print(a.project(b))

	g = Vector3(0,0,1)
	h = Vector3(0,1,0)
	t = 0
	while(True):
		t += 0.001
		print(h.rotate(g, t))
		time.sleep(0.01)


if __name__ == '__main__':
	main()
