from vector import Vector3
import pygame

Width = 1024
Height = 768
scaleFactor = 200


class mesh:
	def __init__(self, polys):
		self.polys = polys

	def translate(self, direction):
		for poly in self.polys:
			poly.translate(direction)

	def drawWF(self, colour, screen):
		for poly in self.polys:
			poly.drawWF(colour, screen)

	def drawPoly(self, colour, screen):
		for poly in self.polys:
			poly.GetZindex() #Get Z index
		self.polys.sort(key=lambda x: x.Zindex, reverse=False) #Sort by Z index

		for poly in self.polys: #Render (Order is correct now)
			poly.drawPoly(colour, screen)

	def rotate(self, axis, theta):
		for poly in self.polys:
			poly.rotate(axis, theta)

	def scale(self, factor):
		for poly in self.polys:
			poly.scale(factor)


class polygon:
	def __init__(self, pos1, pos2, pos3):
		self.poly = [pos1, pos2, pos3]
		self.cam = Vector3(0,0,1) #Direction of camera

	def GetZindex(self):
		z = -100
		for point in self.poly:
			if(point.Z > z):
				z = point.Z
		self.Zindex = z

	def drawPoly(self, colour, screen):
		facing = self.getNormal().project(self.cam); #Flat Shader
		if(facing > 0): #Backface culling
			r = int(colour[0] * (facing+1)/2)
			g = int(colour[1] * (facing+1)/2)
			b = int(colour[2] * (facing+1)/2)
			pygame.draw.polygon(screen, (r,g,b), [[int((Width/2)+self.poly[0].X),int((Height/2)+self.poly[0].Y)],
											 [int((Width/2)+self.poly[1].X),int((Height/2)+self.poly[1].Y)],
											 [int((Width/2)+self.poly[2].X), int((Height/2)+self.poly[2].Y)]]
											 )
	def drawWF(self, colour, screen):
		facing = self.getNormal().project(self.cam); #Flat Shader
		if(facing > 0): #Backface culling
		      pygame.draw.polygon(screen, colour, [[int((Width/2)+self.poly[0].X),int((Height/2)+self.poly[0].Y)],
											 [int((Width/2)+self.poly[1].X),int((Height/2)+self.poly[1].Y)],
											 [int((Width/2)+self.poly[2].X), int((Height/2)+self.poly[2].Y)]]
											 , 1)

	def getNormal(self):
		a = self.poly[0] - self.poly[1]
		b = self.poly[0] - self.poly[2]
		return a.cross(b).normalize()

	def rotate(self, axis, theta):
		for x in range(3):
			self.poly[x] = self.poly[x].rotate(axis, theta)

	def scale(self, factor):
		for x in range(3):
			self.poly[x] = self.poly[x].scale(factor)

	def translate(self, direction):
		for i in range(3):
			self.poly[i] = self.poly[i] + direction


def cube():
	cube = []
	#face1
	cube.append(polygon(Vector3(-0.5,-0.5,0.5), Vector3(0.5,-0.5,0.5), Vector3(0.5,0.5,0.5))) #ccw
	cube.append(polygon(Vector3(-0.5,0.5,0.5), Vector3(-0.5,-0.5,0.5), Vector3(0.5,0.5,0.5))) #ccw

	#face2
	cube.append(polygon(Vector3(0.5,0.5,-0.5), Vector3(0.5,-0.5,-0.5), Vector3(-0.5,-0.5,-0.5))) #ccw
	cube.append(polygon(Vector3(0.5,0.5,-0.5), Vector3(-0.5,-0.5,-0.5), Vector3(-0.5,0.5,-0.5))) #ccw

	#face3
	cube.append(polygon(Vector3(0.5,-0.5,0.5), Vector3(0.5,0.5,-0.5), Vector3(0.5,0.5,0.5)))
	cube.append(polygon(Vector3(0.5,-0.5,0.5), Vector3(0.5,-0.5,-0.5), Vector3(0.5,0.5,-0.5)))

	#face4
	cube.append(polygon(Vector3(-0.5,-0.5,0.5), Vector3(-0.5,0.5,0.5), Vector3(-0.5,0.5,-0.5)))
	cube.append(polygon(Vector3(-0.5,0.5,-0.5), Vector3(-0.5,-0.5,-0.5), Vector3(-0.5,-0.5,0.5)))

	#face5
	cube.append(polygon(Vector3(-0.5,0.5,0.5), Vector3(0.5,0.5,0.5), Vector3(0.5,0.5,-0.5)))
	cube.append(polygon(Vector3(0.5,0.5,-0.5), Vector3(-0.5,0.5,-0.5), Vector3(-0.5,0.5,0.5)))

	#face5
	cube.append(polygon(Vector3(0.5,-0.5,-0.5), Vector3(0.5,-0.5,0.5), Vector3(-0.5,-0.5,0.5)))
	cube.append(polygon(Vector3(-0.5,-0.5,0.5), Vector3(-0.5,-0.5,-0.5), Vector3(0.5,-0.5,-0.5)))

	return mesh(cube)
