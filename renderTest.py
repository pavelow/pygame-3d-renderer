#! /bin/python

import pygame
from vector import Vector3
from objparse import parseOBJ
from mesh import *

import os, sys
import pygame, math, time
from pygame.locals import *


Width = 1024
Height = 768
scaleFactor = 200

z = Vector3(0,0,1)
y = Vector3(0,1,0)
x = Vector3(1,0,0)

cam = Vector3(0,0,1) #Direction of camera

def main():
	if(len(sys.argv) > 1):
		c = parseOBJ(sys.argv[1])
	else:
		c = parseOBJ("cylinder.obj")

	c.scale(scaleFactor)

	print("PyGame Init")

	pygame.init()
	pygame.font.init()
	screen = pygame.display.set_mode((Width, Height))
	screen.fill((0,0,0))

	t = 0
	fps = 0
	while 1:
		start = time.time()
		for event in pygame.event.get():
			if event.type in (QUIT, KEYDOWN):
				if(pygame.key.get_pressed()[pygame.K_ESCAPE]):
					sys.exit()
		if(pygame.key.get_pressed()[pygame.K_UP]):
			c.translate(Vector3(0,-1,0))
		if(pygame.key.get_pressed()[pygame.K_DOWN]):
			c.translate(Vector3(0,1,0))

		if(pygame.key.get_pressed()[pygame.K_LEFT]):
			c.translate(Vector3(-1,0,0))
		if(pygame.key.get_pressed()[pygame.K_RIGHT]):
			c.translate(Vector3(1,0,0))
		#x = x.rotate(z, 0.01)
		screen.fill((0,0,0))
		mouse = pygame.mouse.get_pos()
		#print(mouse)
		t += 1
		#pygame.draw.circle(screen, (255,128,255), (int((Width/2)+x.X),int((Height/2)+x.Y)), 3)
		#pygame.draw.aaline(screen, (255,255,128), (int(Width/2),int(Height/2)), (int((Width/2)+x.X),int((Height/2)+x.Y)), 4)
		c.rotate(y, -((Width/2)-mouse[0])/10000)
		c.rotate(x, ((Height/2)-mouse[1])/10000)
		c.drawPoly((200,200,200), screen)
		#c.drawWF((32,32,32),screen)
		font = pygame.font.Font(None, 20)
		txt = font.render(str("FPS: {0}".format(fps)), 1, (255,255,255))
		screen.blit(txt, (100,100))

		pygame.display.update()
		fps = 1/((time.time()-start))

if __name__ == '__main__':
	main()
