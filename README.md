# pygame-3D-renderer

Renderer using the vector class I wrote for my hexapod.

## Dependencies
- Python3.x
- pygame

## Running
Just run `python3 renderTest.py`  
You can import an arbitrary `*.obj` mesh (assuming it has only triangles) with `python3 renderTest.py <filename>.obj`  

Mouse controls X/Y rotation speed.  
Arrow keys to translate Up/Down, Right/Left  

Not sure how much further this will go,
I think I can speed it up by using numpy, but it's sort of cool having my own vector class underpinning things.
